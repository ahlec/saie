﻿using System;
using System.Collections.Generic;

namespace SaieDictionary.DB
{
    public abstract class DatabaseStoredProcedure
    {
        public abstract String Name { get; }

        public Int32 TimeoutSeconds => 60;

        public Boolean RequiresAuthorization { get; protected set; } = true;

        public abstract IEnumerable<KeyValuePair<String, Object>> GetParameters();
    }
}
