﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading;
using MySql.Data.MySqlClient;
using PubSub;
using SaieDictionary.Auth;
using SaieDictionary.Underpinnings;

namespace SaieDictionary.DB
{
    public sealed class Database
    {
        public static Database Instance => _instance.Value;

        Database()
        {
        }

        public ServerInfo Reconnect()
        {
            if ( _connection != null )
            {
                _connection.Close();
                _connection = null;
                this.Publish<DatabaseDisconnectedEvent>();
            }

            String connectionString = $"server={Settings.Instance.DatabaseHost};uid={Settings.Instance.DatabaseUser};pwd={Settings.Instance.DatabasePassword};database={Settings.Instance.DatabaseSchema};";

            try
            {
                _connection = new MySqlConnection( connectionString );
                _connection.Open();
                this.Publish<DatabaseConnectedEvent>();

                MySqlCommand serverInfoCommand = new MySqlCommand
                {
                    CommandText = "ServerInfo",
                    Connection = _connection,
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = 60
                };

                using ( DbDataReader reader = serverInfoCommand.ExecuteReader() )
                {
                    if ( reader == null || !reader.Read() )
                    {
                        _connection = null;
                        this.Publish( new DatabaseErrorEvent( "Error getting server info",
                            "The ServerInfo stored procedure returned a null reader or an empty result set. Could not connect successfully." ) );
                        return null;
                    }

                    ServerInfo info = new ServerInfo( reader.GetInt32( 0 ) );
                    return info;
                }
            }
            catch ( MySqlException ex )
            {
                _connection = null;
                this.Publish( new DatabaseErrorEvent( ex ) );
                return null;
            }
        }

        public DatabaseQueryableResult<T> Query<T>( DatabaseQueryableProcedure<T> procedure )
        {
            if ( procedure == null )
            {
                throw new ArgumentNullException( nameof( procedure ) );
            }

            MySqlCommand command = new MySqlCommand
            {
                CommandText = procedure.Name,
                Connection = _connection,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = procedure.TimeoutSeconds
            };

            if ( procedure.RequiresAuthorization )
            {
                if ( UserManager.Instance.CurrentUser == null )
                {
                    return new DatabaseQueryableResult<T>( DatabaseErrorCode.RequiresAuthorization );
                }

                command.Parameters.AddWithValue( "user_hash", UserManager.Instance.CurrentUser.Hash );
            }

            foreach ( KeyValuePair<String, Object> parameter in procedure.GetParameters() )
            {
                command.Parameters.AddWithValue( parameter.Key, parameter.Value );
            }

            try
            {
                using ( DbDataReader reader = command.ExecuteReader() )
                {
                    if ( reader == null )
                    {
                        this.Publish( new DatabaseErrorEvent( $"{procedure.Name} returned a null DbDataReader", null ) );
                        return null;
                    }

                    if ( !reader.Read() )
                    {
                        return new DatabaseQueryableResult<T>( DatabaseErrorCode.EmptyReturnSet );
                    }

                    if ( reader.GetName( 0 ) == "error_code" )
                    {
                        DatabaseErrorCode returnedErrorCode = (DatabaseErrorCode) reader.GetInt32( 0 );
                        if ( returnedErrorCode != DatabaseErrorCode.Success )
                        {
                            return new DatabaseQueryableResult<T>( returnedErrorCode );
                        }
                    }

                    return new DatabaseQueryableResult<T>( procedure.Read( reader ) );
                }
            }
            catch ( MySqlException ex )
            {
                this.Publish( new DatabaseErrorEvent( ex ) );
                return new DatabaseQueryableResult<T>( DatabaseErrorCode.MySqlException );
            }
        }

        static readonly Lazy<Database> _instance = new Lazy<Database>( () => new Database(), LazyThreadSafetyMode.ExecutionAndPublication );
        MySqlConnection _connection;
    }
}
