﻿using System;

namespace SaieDictionary.DB
{
    public enum DatabaseErrorCode
    {
        Success = 0,

        // ---------------- General SQL Errors
        MySqlException = 10,
        EmptyReturnSet = 11,

        // ---------------- User Errors
        RequiresAuthorization = 1000, // this procedure requires authorization and the user is not currently logged in
        InvalidUser = 1001, // user doesn't exist in the database or was disabled
        Unauthorized = 1002, // user doesn't have the authorization required to run this procedure
        InvalidUsernameOrPassword = 1003, // provided username or password is incorrect
        UserIsDisabled = 1004, // the specified user account is disabled and cannot be used
    }
}
