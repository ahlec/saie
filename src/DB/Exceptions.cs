﻿using System;

namespace SaieDictionary.DB
{
    public abstract class DatabaseException : Exception
    {
        protected DatabaseException( String message = null ) : base( message )
        {
        }
    }

    public sealed class DatabaseAuthorizationException : DatabaseException
    {
    }

    public sealed class DatabaseCacheException : DatabaseException
    {
        public DatabaseCacheException( String message ) : base( message )
        {
        }
    }

    public class DatabaseProcedureResultMismatchException : DatabaseException
    {
    }
}
