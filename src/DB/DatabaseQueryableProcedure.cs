﻿using System;
using System.Data.Common;

namespace SaieDictionary.DB
{
    public abstract class DatabaseQueryableProcedure<T> : DatabaseStoredProcedure
    {
        public abstract T Read( DbDataReader reader );
    }
}
