﻿using System;
using MySql.Data.MySqlClient;

namespace SaieDictionary.DB
{
    public class DatabaseConnectedEvent
    {
    }

    public class DatabaseDisconnectedEvent
    {
    }

    public class DatabaseErrorEvent
    {
        public DatabaseErrorEvent( MySqlException ex )
        {
            Type = ex?.GetType().Name ?? "General";
            Message = ex?.Message;
        }

        public DatabaseErrorEvent( String type, String message )
        {
            Type = type;
            Message = message;
        }

        public String Type { get; }
        public String Message { get; }
    }
}
