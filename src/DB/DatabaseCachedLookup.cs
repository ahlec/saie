﻿using System;
using System.Collections.Generic;

namespace SaieDictionary.DB
{
    public abstract class DatabaseCachedLookup<TLookupKey, TObject> where TObject : DatabaseCacheableObject<TLookupKey>
    {
        class CacheEntry
        {
            public TObject Object;
            public DateTime ExpirationUTC;
        }

        protected DatabaseCachedLookup( TimeSpan cacheTimeout )
        {
            _cacheTimeout = cacheTimeout;
        }

        public TObject Get( TLookupKey key )
        {
            if ( key == null )
            {
                throw new ArgumentNullException( nameof( key ) );
            }

            CacheEntry cachedObject;
            if ( _cache.TryGetValue( key, out cachedObject ) )
            {
                if ( cachedObject.ExpirationUTC <= DateTime.UtcNow )
                {
                    return cachedObject.Object;
                }

                _cache.Remove( key );
            }

            DatabaseQueryableProcedure<TObject> procedure = GetRetrievalProcedure( key );
            DatabaseQueryableResult<TObject> retrievedObject = Database.Instance.Query( procedure );
            if ( retrievedObject.ErrorCode != DatabaseErrorCode.Success )
            {
                throw new DatabaseCacheException( $"Unable to retrieve from the database (error: {retrievedObject.ErrorCode})" );
            }

            _cache.Add( key, new CacheEntry
            {
                Object = retrievedObject.Value,
                ExpirationUTC = DateTime.UtcNow + _cacheTimeout
            } );
            return retrievedObject.Value;
        }

        protected abstract DatabaseQueryableProcedure<TObject> GetRetrievalProcedure( TLookupKey key );

        readonly TimeSpan _cacheTimeout;
        readonly Dictionary<TLookupKey, CacheEntry> _cache = new Dictionary<TLookupKey, CacheEntry>();
    }
}
