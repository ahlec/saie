﻿using System;

namespace SaieDictionary.DB
{
    public sealed class DatabaseQueryableResult<T>
    {
        public DatabaseQueryableResult( T value )
        {
            Value = value;
        }

        public DatabaseQueryableResult( DatabaseErrorCode errorCode )
        {
            ErrorCode = errorCode;
        }

        public DatabaseErrorCode ErrorCode { get; } = DatabaseErrorCode.Success;

        public T Value { get; }
    }
}
