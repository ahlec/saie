﻿using System;
using System.Threading;

namespace SaieDictionary
{
    public sealed class Settings
    {
        public static Settings Instance => _instance.Value;

        public String DatabaseHost { get; } = "127.0.0.1";
        public String DatabaseUser { get; } = "saie";
        public String DatabasePassword { get; } = "borkins";
        public String DatabaseSchema { get; } = "saie";

        static readonly Lazy<Settings> _instance = new Lazy<Settings>( () => new Settings(), LazyThreadSafetyMode.ExecutionAndPublication );
    }
}
