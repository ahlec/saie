﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Practices.Prism.Mvvm;
using PubSub;
using SaieDictionary.Search;
using SaieDictionary.Wpf;

namespace SaieDictionary.ViewModel
{
    public class HeaderViewModel : BindableBase
    {
        public const String DefaultSearchBoxText = "Enter search here";

        public HeaderViewModel()
        {
            this.Subscribe<WindowGainedFocus>( OnWindowGainedFocus );
            this.Subscribe<WindowLostFocus>( OnWindowLostFocus );
        }

        public Brush BorderBrush => ( _doesWindowHaveFocus ? SystemParameters.WindowGlassBrush : SystemColors.ActiveBorderBrush );

        public String SearchText
        {
            get { return _searchText; }
            set
            {
                SetProperty( ref _searchText, value );
                OnPropertyChanged( () => IsDefaultSearchText );
            }
        }

        public Boolean IsDefaultSearchText => DefaultSearchBoxText.Equals( _searchText, StringComparison.Ordinal );

        void OnWindowGainedFocus( WindowGainedFocus e )
        {
            _doesWindowHaveFocus = true;
            OnPropertyChanged( () => BorderBrush );
        }

        void OnWindowLostFocus( WindowLostFocus e )
        {
            _doesWindowHaveFocus = false;
            OnPropertyChanged( () => BorderBrush );
        }

        #region Commands

        public ICommand SearchCommand =>
            ( _searchCommand ?? ( _searchCommand = new Command( null, OnSearch ) ) );

        void OnSearch( Object o )
        {
            if ( String.IsNullOrWhiteSpace( SearchText ) )
            {
                return;
            }

            this.Publish( new SearchRequested
            {
                Query = SearchText
            } );
        }

        ICommand _searchCommand;
        #endregion

        String _searchText = DefaultSearchBoxText;
        Boolean _doesWindowHaveFocus = true;
    }
}
