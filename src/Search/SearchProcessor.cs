﻿using System;
using System.Threading;
using PubSub;
using SaieDictionary.DB;
using SaieDictionary.Search.Procs;

namespace SaieDictionary.Search
{
    public class SearchProcessor
    {
        public SearchProcessor()
        {
            this.Subscribe<SearchRequested>( OnSearchRequested );
        }

        void OnSearchRequested( SearchRequested request )
        {
            DatabaseQueryableResult<QueryResults> results = Database.Instance.Query( new QueryProcedure( request.Query ) );
            if ( results.ErrorCode != DatabaseErrorCode.Success )
            {
                return;
            }

        }
    }
}
