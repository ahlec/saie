﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SaieDictionary.DB;

namespace SaieDictionary.Search.Procs
{
    public class QueryProcedure : DatabaseQueryableProcedure<QueryResults>
    {
        public QueryProcedure( String query )
        {
            ParseQuery( query );
        }

        public override String Name => "proc_query";

        public override IEnumerable<KeyValuePair<String, Object>> GetParameters()
        {
            yield return new KeyValuePair<string, object>( "search_text", _searchText );
        }

        public override QueryResults Read( DbDataReader reader )
        {
            throw new NotImplementedException();
        }

        void ParseQuery( String query )
        {
            _searchText = query;
        }

        String _searchText;
    }
}
