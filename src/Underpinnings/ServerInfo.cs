﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaieDictionary.Underpinnings
{
    public sealed class ServerInfo
    {
        public ServerInfo( Int32 dbVersion )
        {
            DatabaseVersion = dbVersion;
        }

        public Int32 DatabaseVersion { get; }
    }
}
