﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PubSub;
using SaieDictionary.Auth;
using SaieDictionary.DB;
using SaieDictionary.Underpinnings;
using SaieDictionary.Wpf;

namespace SaieDictionary
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            this.Subscribe<DatabaseConnectedEvent>( OnDatabaseConnected );
            this.Subscribe<DatabaseDisconnectedEvent>( OnDatabaseDisconnected );
            this.Subscribe<DatabaseErrorEvent>( OnDatabaseError );

            UpdateProgramDisabledFlag();
        }

        #region Database

        public Boolean IsConnectedToDatabase
        {
            get { return (Boolean) GetValue( IsConnectedToDatabaseProperty ); }
            set { SetValue( IsConnectedToDatabaseProperty, value ); }
        }

        void OnDatabaseConnected( DatabaseConnectedEvent e )
        {
            IsConnectedToDatabase = true;
        }

        void OnDatabaseDisconnected( DatabaseDisconnectedEvent e )
        {
            IsConnectedToDatabase = false;
        }

        void OnDatabaseError( DatabaseErrorEvent e )
        {
        }

        void AttemptConnectDatabase()
        {
            Database.Instance.Reconnect();
        }

        #endregion

        #region Program Disabling

        public Boolean IsProgramDisabled
        {
            get { return (Boolean) GetValue( IsProgramDisabledProperty ); }
            private set { SetValue( IsProgramDisabledProperty, value ); }
        }

        public String ProgramDisabledReason
        {
            get { return (String) GetValue( ProgramDisabledReasonProperty ); }
            private set { SetValue( ProgramDisabledReasonProperty, value ); }
        }

        public String ProgramDisabledButtonText
        {
            get { return (String) GetValue( ProgramDisabledButtonTextProperty ); }
            private set { SetValue( ProgramDisabledButtonTextProperty, value ); }
        }

        public ProgramDisabledRectifyCommand ProgramDisabledButtonCommand => ( _programDisabledCommand ?? ( _programDisabledCommand = new ProgramDisabledRectifyCommand() ) );

        public class ProgramDisabledRectifyCommand : ICommand
        {
            public Action Rectification
            {
                get { return _rectification; }
                set
                {
                    _rectification = value;
                    CanExecuteChanged?.Invoke( this, EventArgs.Empty );
                }
            }

            public void Execute( Object o )
            {
                _rectification?.Invoke();
            }

            public Boolean CanExecute( Object o )
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;

            Action _rectification;
        }

        void UpdateProgramDisabledFlag()
        {
            if ( !IsConnectedToDatabase )
            {
                IsProgramDisabled = true;
                ProgramDisabledReason = "Not currently connected to the database.";
                ProgramDisabledButtonText = "Reconnect";
                ProgramDisabledButtonCommand.Rectification = AttemptConnectDatabase;
                return;
            }

            IsProgramDisabled = false;
            ProgramDisabledReason = null;
        }

        static void OnProgramDisablingPropertyChanged( DependencyObject o, DependencyPropertyChangedEventArgs e )
        {
            MainWindow window = (MainWindow) o;
            window.UpdateProgramDisabledFlag();
        }

        public static readonly DependencyProperty IsProgramDisabledProperty = DependencyProperty.Register( "IsProgramDisabled", typeof( Boolean ),
            typeof( MainWindow ) );
        public static readonly DependencyProperty ProgramDisabledReasonProperty = DependencyProperty.Register( "ProgramDisabledReason",
            typeof( String ), typeof( MainWindow ) );
        public static readonly DependencyProperty ProgramDisabledButtonTextProperty = DependencyProperty.Register( "ProgramDisabledButtonText",
            typeof( String ), typeof( MainWindow ) );

        ProgramDisabledRectifyCommand _programDisabledCommand;

        #endregion

        protected override void OnActivated( EventArgs e )
        {
            base.OnActivated( e );
            this.Publish( new WindowGainedFocus() );
        }

        protected override void OnDeactivated( EventArgs e )
        {
            base.OnDeactivated( e );
            this.Publish( new WindowLostFocus() );
        }

        public static readonly DependencyProperty SimpleTextProperty = DependencyProperty.Register( "SimpleText", typeof( String ),
            typeof( MainWindow ) );
        public static readonly DependencyProperty IsConnectedToDatabaseProperty = DependencyProperty.Register( "IsConnectedToDatabase",
            typeof( Boolean ), typeof( MainWindow ), new PropertyMetadata { PropertyChangedCallback = OnProgramDisablingPropertyChanged } );
    }
}
