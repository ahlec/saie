﻿using System;

namespace SaieDictionary.Saie
{
    [Flags]
    public enum Usage
    {
        Noun,
        Verb,
        Adjective,
        Pronoun,
        Particle,
        Identifier,
        Modifier,
        Adjunctor
    }
}
