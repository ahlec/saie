﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaieDictionary.Auth;

namespace SaieDictionary.Saie
{
    public abstract partial class SaieBase
    {
        public User Creator { get; }

        public DateTime Created { get; }

        public User LastEditor { get; private set; }

        public DateTime LastEdited { get; private set; }
    }
}
