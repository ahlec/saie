﻿using System;
using System.Threading;
using SaieDictionary.Auth.Procs;
using SaieDictionary.DB;

namespace SaieDictionary.Auth
{
    public sealed class UserManager : DatabaseCachedLookup<String, User>
    {
        public static UserManager Instance => _instance.Value;

        UserManager() :  base( TimeSpan.FromHours( 1 ) )
        {
        }

        public User CurrentUser { get; private set; }

        public LoginResult Login( String username, String password )
        {
            if ( CurrentUser != null )
            {
                Logout();
            }

            LoginProcedure procedure = new LoginProcedure( username, password );
            DatabaseQueryableResult<User> loginResult = Database.Instance.Query( procedure );
            switch ( loginResult.ErrorCode )
            {
                case DatabaseErrorCode.Success:
                    {
                        CurrentUser = loginResult.Value;
                        return LoginResult.Success;
                    }
                case DatabaseErrorCode.InvalidUsernameOrPassword:
                    {
                        return LoginResult.InvalidUsernameOrPassword;
                    }
                case DatabaseErrorCode.UserIsDisabled:
                    {
                        return LoginResult.AccountIsDisabled;
                    }
                default:
                    {
                        throw new DatabaseProcedureResultMismatchException();
                    }
            }
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }

        protected override DatabaseQueryableProcedure<User> GetRetrievalProcedure( String key )
        {
            return new GetUserByHashProcedure( key );
        }

        static readonly Lazy<UserManager> _instance = new Lazy<UserManager>( () => new UserManager(), LazyThreadSafetyMode.ExecutionAndPublication );
    }
}
