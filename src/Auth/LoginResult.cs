﻿using System;

namespace SaieDictionary.Auth
{
    public enum LoginResult
    {
        Success,
        InvalidUsernameOrPassword,
        AccountIsDisabled
    }
}
