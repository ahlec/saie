﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Security.Cryptography;
using System.Text;
using SaieDictionary.DB;

namespace SaieDictionary.Auth.Procs
{
    public sealed class LoginProcedure : DatabaseQueryableProcedure<User>
    {
        public LoginProcedure( String username, String password )
        {
            RequiresAuthorization = false;

            if ( username == null )
            {
                throw new ArgumentNullException( nameof( username ) );
            }

            _username = username;

            if ( password == null )
            {
                throw new ArgumentNullException( nameof( password ) );
            }

            Byte[] inputBytes = Encoding.UTF8.GetBytes( password );
            SHA256 sha256 = SHA256.Create();
            Byte[] encodedBytes = sha256.ComputeHash( inputBytes );
            StringBuilder builder = new StringBuilder();
            foreach ( Byte encodedByte in encodedBytes )
            {
                builder.AppendFormat( "{0:x2}", encodedByte );
            }
            _password = builder.ToString();
        }

        public override String Name => "proc_login";

        public override IEnumerable<KeyValuePair<String, Object>> GetParameters()
        {
            yield return new KeyValuePair<String, Object>( "provided_username", _username );
            yield return new KeyValuePair<string, object>( "provided_hashed_password", _password );
        }

        public override User Read( DbDataReader reader )
        {
            return new User
            {
                Username = reader.GetString( 0 ),
                AccountCreatedUTC = reader.GetDateTime( 1 ),
                LastLoginUTC = reader.GetDateTime( 2 ), // by virtue of this being a login function, this will never be null
                IsDisabled = reader.GetBoolean( 3 ),
                Hash = reader.GetString( 4 ),
                AuthLevel = (AuthorizationLevel) reader.GetInt32( 5 )
            };
        }

        readonly String _username;
        readonly String _password;
    }
}
