﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using SaieDictionary.DB;

namespace SaieDictionary.Auth.Procs
{
    public sealed class GetUserByHashProcedure : DatabaseQueryableProcedure<User>
    {
        public GetUserByHashProcedure( String userHash )
        {
            _userHash = userHash;
        }

        public override String Name => "proc_get_user_by_hash";

        public override IEnumerable<KeyValuePair<String, Object>> GetParameters()
        {
            yield return new KeyValuePair<String, Object>( "provided_user_hash", _userHash );
            yield return new KeyValuePair<string, object>( "desired_user_hash", _userHash );
        }

        public override User Read( DbDataReader reader )
        {
            return new User
            {
                Username = reader.GetString( 0 ),
                AccountCreatedUTC = reader.GetDateTime( 1 ),
                LastLoginUTC = ( reader.IsDBNull( 2 ) ? null : (DateTime?) reader.GetDateTime( 2 ) ),
                IsDisabled = reader.GetBoolean( 3 ),
                Hash = reader.GetString( 4 ),
                AuthLevel = (AuthorizationLevel) reader.GetInt32( 5 )
            };
        }

        readonly String _userHash;
    }
}
