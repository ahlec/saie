﻿using System;
using SaieDictionary.DB;

namespace SaieDictionary.Auth
{
    public sealed class User : DatabaseCacheableObject<String>
    {
        public String Username { get; set; }

        public DateTime AccountCreatedUTC { get; set; }

        public DateTime? LastLoginUTC { get; set; }

        public Boolean IsDisabled { get; set; }

        public String Hash { get; set; }

        public AuthorizationLevel AuthLevel { get; set; }
    }
}
