﻿using System;

namespace SaieDictionary.Auth
{
    public enum AuthorizationLevel
    {
        None,
        Basic,
        Editor,
        Administrator
    }
}
